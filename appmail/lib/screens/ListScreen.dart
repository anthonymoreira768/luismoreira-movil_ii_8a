import 'package:flutter/material.dart';
import 'DetailScreen.dart';
import '../model/backend.dart';
import '../model/email.dart';


class ListScreen extends StatefulWidget {
  @override
  _ListScreenState createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {
  List<Email> emails = Backend().getEmails();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('MOCK MAIL'),
          centerTitle: true,
        ),
        body: ListView.builder(
            itemCount: emails.length,
            itemBuilder: (BuildContext context, int index) {
              final email = emails[index];
              final id = email.id;
              final bool read = email.read;
              return Dismissible(
                key: ValueKey(id),
                direction: DismissDirection.endToStart,
                background: Container(
                  color: Colors.grey,

                  child: Icon(
                    Icons.delete_rounded,
                    color: Colors.black,
                    size: 40,
                  ),
                ),
                
                child: Container(
                  child: Column(
                    children: [
                      
                      ListTile(
                        
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            
                            Text(
                              email.from,
                              style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 13),
                            ),
                            Text(
                        email.dateTime.toString(),
                        style: TextStyle(color: Colors.black, fontSize: 10),
                      ),
                          ],                         
                        ), 
                                              
                        subtitle: Container(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                 Icon(
                                    read
                                        ? Icons.brightness_1_outlined
                                        : Icons.brightness_1,
                                    color: Colors.greenAccent,
                                ),
                                Text(email.subject,
                                    style: TextStyle(
                                      fontSize: 15,
                                    )), 
                              ]),
                        ),
                        onLongPress: () {
                          Backend().markEmailAsRead(id);
                          setState(() {});
                        },
                        onTap: () {
                          Backend().markEmailAsRead(id);
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => DetailScreen(
                                    email: emails[index],
                                  )));
                          setState(() {});
                        },
                      ),
                      Divider(color: Colors.lightBlueAccent),
                    ],
                  ),
                ),
                onDismissed: (direction) {
                  setState(() {
                    emails.removeAt(index);
                    Backend().deleteEmail(id);
                    print(toString());
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        duration: Duration(seconds: 1),
                        content: Text( "$id")));
                    setState(() {});
                  });
                },
              );
            }));
  }
}
